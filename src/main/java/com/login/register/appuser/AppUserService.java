package com.login.register.appuser;

import com.login.register.registration.token.ConfirmationToken;
import com.login.register.registration.token.ConfirmationTokenService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AppUserService implements UserDetailsService {

    private final static String USER_NOT_FOUND = "user with email %s not found";
    private final AppUserRepository appUserRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final ConfirmationTokenService confirmationTokenService;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return appUserRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException(String.format(USER_NOT_FOUND, email)));
    }

    // method untuk user melakukan sign up
    public String signUpUser(AppUser appUser) {
        // check if user exist
        final boolean usertExists = appUserRepository
                .findByEmail(appUser.getEmail())
                .isPresent();

        if (usertExists) {
            // email sudah terdaftar
            throw new IllegalStateException("email already taken");
        }

        // encrypt dulu password sebelum disimpan ke table database
        final String encodePassword = bCryptPasswordEncoder.encode(appUser.getPassword());

        appUser.setPassword(encodePassword);

        appUserRepository.save(appUser);

        // create token
        String token = UUID.randomUUID().toString();

        // confirmation token
        ConfirmationToken confirmationToken = new ConfirmationToken(
                token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(15),
                appUser
        );

        // save token
        confirmationTokenService.saveConfirmationToken(confirmationToken);

        // TODO: SEND EMAIL

        // return the token
        return token;
    }

    public void enableAppUser(String email) {
    }
}
